const str = "[232,2330223,2323,23233,534][23423,234234,2221]";

class ChrSon {
    constructor()
    {
        this.parse = this.Parse;
        this.stringify = this.Stringify;
    }

    Parse(str)
    {
        const sliced = str.slice(0);
        const len = sliced.length;

        var farr = [];
        var lastchar;
        var curslot;
        var curnbr = "";

        for (let i = 0; i < len; i++)
        {
            if (sliced[i - 1])
            {
                lastchar = sliced[i - 1];
            }

            switch (sliced[i])
            {
                case "[":
                    curslot = farr.length;

                    farr[curslot] = [];

                    break;
                case "]":
                    if (!lastchar)
                        throw "Invalid string to parse cannot start woth ''"

                        farr[curslot][farr[curslot].length] = parseInt(curnbr);
                        curnbr = "";

                    break;
                case ",":
                    farr[curslot][farr[curslot].length] = parseInt(curnbr);
                    curnbr = "";

                    break;

                default:
                    if (lastchar)
                        curnbr += sliced[i];
                    else
                        throw "Invalid string to parse, wrong format"
            }
        }

        return farr;
    }

    Stringify(arr)
    {
        const len = arr.length;
        var fstr = "";

        for (let i = 0; i < len; i++)
        {
            const slen = arr[i].length
            fstr += "[";

            for (let ii = 0; ii < slen; ii++)
            {
                if (arr[i][ii + 1])
                    fstr += arr[i][ii] + ",";
                else
                    fstr += arr[i][ii] + "]";
            }
        }

        return fstr;
    }
}

const CSON = new ChrSon();
//console.log(CSON.Stringify(CSON.parse(str)))
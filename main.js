const {app, BrowserWindow} = require('electron');
const {ipcMain} = require('electron');
var PE = { maximized: false }

let mainWindow;

function createWindow()
{
    mainWindow = new BrowserWindow(
        {
            width: 1280,
            height: 720,
            resizable: false,
            fullscreen: false,
            frame: false,
            backgroundColor: "#222"
        }
    );

    mainWindow.loadFile("index.html");
    mainWindow.toggleDevTools();
    mainWindow.setMenu(null);

    mainWindow.on('closed', function ()
    {
        mainWindow = null;
    })
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin')
    {
        app.quit();
    }
})

app.on('activate', () => {
    if (mainWindow === null)
    {
        createWindow();
    }
});

ipcMain.on("klosieren", (event) => {
    app.quit();
});


ipcMain.on("pleinecran", (event) => {
    if(PE.maximized == false)
    {
        PE.maximized = true;
        mainWindow.maximize();
    }
    else 
    {
        PE.maximized = false;
        mainWindow.unmaximize();
    }
});


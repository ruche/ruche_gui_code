$(() => {
    const images = [
        "follow",
        "home",
        "stats"
    ];

    var loadedImages = new Array();

    for (let i = 0; i < images.length; i++)
    {
        loadedImages[i] = new Image();
        loadedImages[i].src = "./files/img/" + images[i] + "img.jpg";
    }

    const {ipcRenderer} = require('electron');

    const mainco = document.getElementById("main");
    const loadco = document.getElementById("loading");
    const curstatsco = document.getElementById("curstats");
    const homeco = document.getElementById("home");
    const followco = document.getElementById("followstats");
    const masstxt = document.getElementById("masstxt");
    const avgcount = document.getElementById("avgvi");

    const backg = document.getElementById("background");

    const chrthing = document.getElementById("chrfreq");

    var currentBtn = "homebutton";

    var inhome = true;
    var hygrobar;
    var hygrointbar;
    var tempbar;
    var tempintbar;

    hoveritem = function(div)
    {
        if (div.className != currentBtn)
            div.childNodes[1].style.opacity = 1;
    }

    outitem = function(div)
    {
        if (div.className != currentBtn)
            div.childNodes[1].style.opacity = 0;
    }

    setTimeout(() => {
        loadco.style.opacity = 1;

        particlesJS("particleCanvas-Blue", {
            particles: {
                number: {
                    value: 100,
                    density: {
                        enable: true,
                        value_area: 800
                    }
                },
                color: {
                    value: "#1B5F70"
                },
                shape: {
                    type: "polygon",
                    stroke: {
                        width: 0,
                        color: "#000000"
                    },
                    polygon: {
                        nb_sides: 6
                    },
                    image: {
                        src: "img/github.svg",
                        width: 100,
                        height: 100
                    }
                },
                opacity: {
                    value: 0.5,
                    random: false,
                    anim: {
                        enable: true,
                        speed: 1,
                        opacity_min: 0.1,
                        sync: false
                    }
                },
                size: {
                    value: 10,
                    random: true,
                    anim: {
                        enable: false,
                        speed: 10,
                        size_min: 0.1,
                        sync: false
                    }
                },
                line_linked: {
                    enable: false,
                    distance: 150,
                    color: "#ffffff",
                    opacity: 0.4,
                    width: 1
                },
                move: {
                    enable: true,
                    speed: 0.5,
                    direction: "none",
                    random: true,
                    straight: false,
                    out_mode: "bounce",
                    bounce: false,
                    attract: {
                        enable: false,
                        rotateX: 394.57382081613633,
                        rotateY: 157.82952832645452
                    }
                }
            },
            interactivity: {
                detect_on: "canvas",
                events: {
                    onhover: {
                        enable: true,
                        mode: "grab"
                    },
                    onclick: {
                        enable: false,
                        mode: "push"
                    },
                    resize: true
                },
                modes: {
                    grab: {
                        distance: 200,
                        line_linked: {
                            opacity: 0.2
                        }
                    },
                    bubble: {
                        distance: 1500,
                        size: 40,
                        duration: 7.272727272727273,
                        opacity: 0.3676323676323676,
                        speed: 3
                    },
                    repulse: {
                        distance: 50,
                        duration: 0.4
                    },
                    push: {
                        particles_nb: 4
                    },
                    remove: {
                        particles_nb: 2
                    }
                }
            },
            retina_detect: true
        });
        
        particlesJS("particleCanvas-White", {
            particles: {
                number: {
                    value: 250,
                    density: {
                        enable: true,
                        value_area: 800
                    }
                },
                color: {
                    value: "#ffffff"
                },
                shape: {
                    type: "polygon",
                    stroke: {
                        width: 0,
                        color: "#000000"
                    },
                    polygon: {
                        nb_sides: 6
                    },
                    image: {
                        src: "img/github.svg",
                        width: 100,
                        height: 100
                    }
                },
                opacity: {
                    value: 0.5,
                    random: true,
                    anim: {
                        enable: false,
                        speed: 0.2,
                        opacity_min: 0,
                        sync: false
                    }
                },
                size: {
                    value: 15,
                    random: true,
                    anim: {
                        enable: true,
                        speed: 8,
                        size_min: 0.1,
                        sync: false
                    }
                },
                line_linked: {
                    enable: false,
                    distance: 150,
                    color: "#ffffff",
                    opacity: 0.4,
                    width: 1
                },
                move: {
                    enable: true,
                    speed: 0.5,
                    direction: "none",
                    random: true,
                    straight: false,
                    out_mode: "bounce",
                    bounce: false,
                    attract: {
                        enable: true,
                        rotateX: 3945.7382081613637,
                        rotateY: 157.82952832645452
                    }
                }
            },
            interactivity: {
                detect_on: "canvas",
                events: {
                    onhover: {
                        enable: false,
                        mode: "grab"
                    },
                    onclick: {
                        enable: false,
                        mode: "push"
                    },
                    resize: true
                },
                modes: {
                    grab: {
                        distance: 200,
                        line_linked: {
                            opacity: 0.2
                        }
                    },
                    bubble: {
                        distance: 1500,
                        size: 40,
                        duration: 7.272727272727273,
                        opacity: 0.3676323676323676,
                        speed: 3
                    },
                    repulse: {
                        distance: 50,
                        duration: 0.4
                    },
                    push: {
                        particles_nb: 4
                    },
                    remove: {
                        particles_nb: 2
                    }
                }
            },
            retina_detect: true
        });

        setTimeout(() => {
            loadco.style.opacity = 0;
            mainco.style.opacity = 1;

            setTimeout(() => {
                backg.style.opacity = 1;
            }, 500);

            particlesJS("particlesjs", {
                "particles": {
                    "number": {
                    "value": 90,
                    "density": {
                        "enable": true,
                        "value_area": 800
                    }
                    },
                    "color": {
                    "value": "#ffffff"
                    },
                    "shape": {
                    "type": "polygon",
                    "stroke": {
                        "width": 0,
                        "color": "#000"
                    },
                    "polygon": {
                        "nb_sides": 6
                    },
                    "image": {
                        "src": "img/github.svg",
                        "width": 100,
                        "height": 100
                    }
                    },
                    "opacity": {
                    "value": 0.25,
                    "random": true,
                    "anim": {
                        "enable": false,
                        "speed": 1,
                        "opacity_min": 0.1,
                        "sync": false
                    }
                    },
                    "size": {
                    "value": 8.306820730501814,
                    "random": true,
                    "anim": {
                        "enable": true,
                        "speed": 10,
                        "size_min": 40,
                        "sync": false
                    }
                    },
                    "line_linked": {
                    "enable": false,
                    "distance": 132.90913168802902,
                    "color": "#ffffff",
                    "opacity": 1,
                    "width": 2
                    },
                    "move": {
                    "enable": true,
                    "speed": 0.9,
                    "direction": "none",
                    "random": false,
                    "straight": false,
                    "out_mode": "out",
                    "bounce": false,
                    "attract": {
                        "enable": false,
                        "rotateX": 600,
                        "rotateY": 1200
                    }
                    }
                },
                "interactivity": {
                    "detect_on": "canvas",
                    "events": {
                    "onhover": {
                        "enable": false,
                        "mode": "grab"
                    },
                    "onclick": {
                        "enable": false,
                        "mode": "push"
                    },
                    "resize": true
                    },
                    "modes": {
                    "grab": {
                        "distance": 400,
                        "line_linked": {
                        "opacity": 1
                        }
                    },
                    "bubble": {
                        "distance": 400,
                        "size": 40,
                        "duration": 2,
                        "opacity": 8,
                        "speed": 3
                    },
                    "repulse": {
                        "distance": 200,
                        "duration": 0.4
                    },
                    "push": {
                        "particles_nb": 4
                    },
                    "remove": {
                        "particles_nb": 2
                    }
                    }
                },
                "retina_detect": true
            });

            setTimeout(() => {
                homeco.style.opacity = 1;
            }, 250);
        }, 5000);
    }, 1000);

    $(() => {
        hygrobar = new ProgressBar.Circle(hygrobr, {
            color: '#aaa',
            strokeWidth: 4,
            trailWidth: 1,
            easing: 'linear',
            duration: 2500,
            background: '#aaa',
            text: {
                autoStyleContainer: false,
                style: {
                    position: 'absolute',
                    left: '15px',
                    top: '65px',
                    padding: 0,
                    margin: 0,
                    "flex-direction": "column",
                    "justify-content": "center",
                    "align-items": "center",
                    "display": "flex",
                    color: "white",
                }
            },
            from: { color: '#e84a37', width: 2 },
            to: { color: '#4CAF50', width: 3 },
            fill: 'rgba(0, 0, 0, 0.5)',

            step: function(state, circle)
            {
                circle.path.setAttribute('stroke', state.color);
                circle.path.setAttribute('stroke-width', state.width);

                circle.setText("Exterieur : " + Math.round(circle.value() * 100) + "%");
            }
        });

        hygrointbar = new ProgressBar.Circle(hygrointbr, {
            color: '#aaa',
            strokeWidth: 4,
            trailWidth: 1,
            easing: 'linear',
            duration: 2500,
            background: '#aaa',
            text: {
                autoStyleContainer: true,
                style: {
                    position: 'absolute',
                    left: '20px',
                    top: '65px',
                    padding: 0,
                    margin: 0,
                    "flex-direction": "column",
                    "justify-content": "center",
                    "align-items": "center",
                    "display": "flex",
                    color: "white",
                }
            },
            from: { color: '#e84a37', width: 2 },
            to: { color: '#4CAF50', width: 3 },
            fill: 'rgba(0, 0, 0, 0.5)',

            step: function(state, circle)
            {
                circle.path.setAttribute('stroke', state.color);
                circle.path.setAttribute('stroke-width', state.width);

                circle.setText("Interieur : " + Math.round(circle.value() * 100) + "%");
            }
        });

        tempbar = new ProgressBar.Circle(tempbr, {
            color: '#aaa',
            strokeWidth: 4,
            trailWidth: 1,
            easing: 'linear',
            duration: 2500,
            background: '#aaa',
            text: {
                autoStyleContainer: false,
                style: {
                    position: 'absolute',
                    left: '15px',
                    top: '65px',
                    padding: 0,
                    margin: 0,
                    "flex-direction": "column",
                    "justify-content": "center",
                    "align-items": "center",
                    "display": "flex",
                    color: "white",
                }
            },
            from: { color: '#e84a37', width: 2 },
            to: { color: '#4CAF50', width: 3 },
            fill: 'rgba(0, 0, 0, 0.5)',

            step: function(state, circle)
            {
                circle.path.setAttribute('stroke', state.color);
                circle.path.setAttribute('stroke-width', state.width);

                circle.setText("Exterieur : " + Math.round(circle.value() * 100) + "°C");
            }
        });

        tempintbar = new ProgressBar.Circle(tempintbr, {
            color: '#aaa',
            strokeWidth: 4,
            trailWidth: 1,
            easing: 'linear',
            duration: 2500,
            background: '#aaa',
            text: {
                autoStyleContainer: true,
                style: {
                    position: 'absolute',
                    left: '20px',
                    top: '65px',
                    padding: 0,
                    margin: 0,
                    "flex-direction": "column",
                    "justify-content": "center",
                    "align-items": "center",
                    "display": "flex",
                    color: "white",
                }
            },
            from: { color: '#e84a37', width: 2 },
            to: { color: '#4CAF50', width: 3 },
            fill: 'rgba(0, 0, 0, 0.5)',

            step: function(state, circle)
            {
                circle.path.setAttribute('stroke', state.color);
                circle.path.setAttribute('stroke-width', state.width);

                circle.setText("Interieur : " + Math.round(circle.value() * 100) + "°C");
            }
        });

        setTimeout(() => {
            const mnthly = ActualData.Montly;

            const generatefield = () => {
                let arr = [];

                for (let i = 0; i < 30; i++)
                    arr[i] = Math.floor(Math.random() * 30);

                return arr;
            };

            const generateHygro = () => {
                let arr = [];

                for (let i = 0; i < mnthly.length; i++)
                    arr[i] = mnthly[i].hygro.i;

                return arr;
            };

            const generateTemp = () => {
                let arr = [];

                for (let i = 0; i < mnthly.length; i++)
                    arr[i] = mnthly[i].temp.i;

                return arr;
            };

            const generateMasse = () => {
                let arr = [];

                for (let i = 0; i < mnthly.length; i++)
                    arr[i] = mnthly[i].masse.vald;

                return arr;
            };

            const generateFreq = () => {
                let arr = [];

                for (let i = 0; i < mnthly.length; i++)
                    arr[i] = mnthly[i].freq.avg / 100;

                return arr;
            };

            const generatelabels = () => {
                let arr = [];

                for (let i = 0; i < mnthly.length; i++)
                    arr[i] = i + 1;

                return arr;
            };

            const ctx = document.getElementById("ingraph");
            const myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: generatelabels(),
                    datasets: [{
                        label: 'Hygrométrie',
                        data: generateHygro(),
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                        ],
                        borderWidth: 1
                    },
                    {
                        label: 'Masse',
                        data: generateMasse(),
                        backgroundColor: [
                            'rgba(82, 192, 75, 0.2)',
                        ],
                        borderColor: [
                            'rgba(82, 192, 75, 1)',
                        ],
                        borderWidth: 1
                    },
                    {
                        label: 'Temperature',
                        data: generateTemp(),
                        backgroundColor: [
                            'rgba(75, 192, 192, 0.2)',
                        ],
                        borderColor: [
                            'rgba(75, 192, 192, 1)',
                        ],
                        borderWidth: 1
                    },
                    {
                        label: 'Frequentation (1/100)',
                        data: generateFreq(),
                        backgroundColor: [
                            'rgba(221, 139, 44, 0.2)',
                        ],
                        borderColor: [
                            'rgba(221, 139, 44, 1)',
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    },
                    legend: {
                        labels: {
                            fontColor: 'white'
                        }
                    }
                }
            });
        }, 2000);

        const ctxjk = document.getElementById("jkgraph");
        const myChartjk = new Chart(ctxjk, {
            type: 'bubble',
            data: {
                labels: "",
                datasets: [
                    {
                        label: ["Masse"],
                        backgroundColor: "rgba(102,210,214,0.4)",
                        borderColor: "rgba(102,210,214,1)",
                        data: [{
                            x: ActualData.data.masse.x,
                            y: ActualData.data.masse.y,
                            r: 15
                        }]
                    },
                ]
            },
            scaleFontColor: "#E2E2E2",
            options: {
            showTooltips: false,
            title: {
                display: true,
                fontColor: '#E2E2E2',
                text: 'Répartition de la masse de la ruche'
            }, scales: {
                yAxes: [{ 
                    scaleLabel: {
                        display: false,
                        labelString: "Happiness"
                    },
                    ticks: {
                        beginAtZero:true,
                        max : 1,
                        min : 0,
                        fontColor: '#E2E2E2',
                    }
                    }],
                    xAxes: [{ 
                    scaleLabel: {
                        display: false,
                        labelString: "GDP (PPP)",
                    },
                    ticks: {
                        beginAtZero:true,
                        max : 1,
                        min : 0,
                        fontColor: '#E2E2E2',
                        labelBackgroundColor: "#E2E2E2",
                    }
                }]
            }
            },
            legend: {
                labels: {
                    fontColor: 'white'
                }
            },
        });
    });

    $('.toggle').on('click', () => {
        $('.menu').toggleClass('expanded');
        $('span').toggleClass('hidden');
        $('.container , .toggle').toggleClass('close');
        //$("#black").fadeOut();
    });

    chkhome = function(cb)
    {
        if (inhome)
        {
            homeco.style.opacity = 0;
            inhome = false;
        }
        else
        {
            cb();
        }
    }

    $(() => {
        $('.homebutton').click(() => {
            homeco.style.opacity = 1;
            curstatsco.style.opacity = 0;
            followco.style.opacity = 0;

            backg.style.backgroundImage = "url(files/img/homeimg.jpg)";

            changeItem("homebutton");
            inhome = true;
        });

        $(".statbtn").click(() => {
            const stats = ActualData.ConvertedToOne();
            console.log(stats);

            chkhome(() => {
                followco.style.opacity = 0;
            });

            curstatsco.style.opacity = 1;

            hygrobar.animate(stats.he);
            hygrointbar.animate(stats.hi);
            tempbar.animate(stats.te);
            tempintbar.animate(stats.ti);

            masstxt.innerHTML = ActualData.data.masse.vald + " Kg";
            avgcount.innerHTML = "Moyenne : " + ActualData.data.freq.avg;

            backg.style.backgroundImage = "url(files/img/statsimg.jpg)";
            changeItem("statbtn");
        });

        WClient.On("updatedata", (DATA) => {
            ActualData.data = DATA;
            const stats = ActualData.ConvertedToOne();

            if (currentBtn == "statbtn")
            {
                hygrobar.animate(stats.he);
                hygrointbar.animate(stats.hi);
                tempbar.animate(stats.te);
                tempintbar.animate(stats.ti);
            }

            masstxt.innerHTML = ActualData.data.masse.vald + " Kg";
            avgcount.innerHTML = "Moyenne : " + ActualData.data.freq.avg;
        });

        $(".follow").click(() => {
            chkhome(() => {
                curstatsco.style.opacity = 0;
            });

            followco.style.opacity = 1;

            backg.style.backgroundImage = "url(files/img/followimg.jpg)";
            changeItem("follow");
        });

        changeItem = function(item)
        {
            document.getElementsByClassName(currentBtn)[0].childNodes[1].style.opacity = 0;

            currentBtn = item;
            document.getElementsByClassName(currentBtn)[0].childNodes[1].style.opacity = 1;
        }

        changeItem("homebutton");

        $("#dascroix").click(() => {
            ipcRenderer.send("klosieren");
        });

        $("#daspleinecran").click(() => {
            ipcRenderer.send("pleinecran");
        });


        $("#masseplus").click(() => {
            $(".overlay").addClass("is-open");
        });

        $(".close-btn").click(() => {
            $('.overlay').removeClass('is-open');
        });

        $("#counterplus").click(() => {
            let ht = "<div id=chritems>";

            for (let i = 0; i < chrdata.length; i++)
            {
                ht += "<div id=chrdataitem><div id=itemtxt>"+ chrdata[i] +"</div>\n</div>\n";
            }

            ht += "</div>";
            chrthing.innerHTML = ht;

            $(".overlay2").addClass("is-open2");
        });

        $(".close-btn2").click(() => {
            $('.overlay2').removeClass('is-open2');
        });
    });
});
class ActualSys {
    constructor()
    {
        this.data = {
            hygro: {i: 0.0, e: 0.0},
            temp: {i: 0.0, e: 0.0},
            freq: {avg: 0.0, arr: []},
            masse: {vald: 0.0, x: 0.0, y: 0.0},
        }

        this.Montly = [];
    }

    ConvertedToOne()
    {
        return {
            hi: this.data.hygro.i / 100,
            he: this.data.hygro.e / 100,
            ti: this.data.temp.i / 100,
            te: this.data.temp.e / 100,
            c: this.data.freq.avg / 100,
            w: this.data.masse.vald / 100
        }
    }
}

var chrdata = [];

const ActualData = new ActualSys;

const WClient = new WebSocketWebClient("localhost", 8080);

WClient.On("connected", () => {
    console.log("Connected to server");
});

WClient.On("firstdata", (DATA) => {
    ActualData.data = DATA;

    for (let i = 0; i < 40; i++)
        chrdata[i] = DATA.freq.arr[i];
});

WClient.On("dailydata", (DDATA) => {
    console.log(DDATA)
    ActualData.Montly = DDATA;
});